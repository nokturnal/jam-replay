
;unknown author

		XDEF	_relocateProgram


ph_branch       EQU $00           ; Start branch in header    [Headerinfos]
ph_tlen         EQU $04-2         ; Lenght of TEXT segment
ph_dlen         EQU $08-2         ; Length DATA segment
ph_blen         EQU $0C-2         ; Length  BSS segment
ph_slen         EQU $10-2         ; Length  Symbol table
ph_res1         EQU $14-2         ; reserved
ph_res2         EQU $18-2         ;     
ph_flag         EQU $1C-2         
ph_len		EQU ph_flag+2

_relocateProgram:
        ; prg address in a0
	move.l	4(sp),a0
        move.l  ph_tlen(A0),D0      ; Lenght of TEXT segment
        add.l   ph_dlen(A0),D0      ; + Length DATA segment
        add.l   ph_slen(A0),D0      ; + Length of symbol table
        lea     ph_len(A0,D0.l),A1  ; Startadresse Reloziertabelle
        lea     ph_len(A0),A0       ; Start address of TEXT Segment
        move.l  A0,D0               ; Relocation of base address
        move.l  (A1)+,D1            ; First relocated word (32 Bit Offset)
        beq.b   end_rel             ; =0 => no relocation
        adda.l  D1,A0               ; richtige Adresse.
        add.l   D0,(A0)             ; Offset draufknallen
        moveq   #0,D1               ; als Wortoffset gebraucht
        moveq   #1,D2               ; Vgl. auf 1 optimieren
rel_loop: 
	move.b  (A1)+,D1            ; Byteoffset holen
        beq    end_rel              ; Nulloffset=Ende der šbertragung
        cmp.b   D2,D1               ; d1=1
        beq.b   add_254             ; add 254 bytes
        adda.l  D1,A0               ; auf n„chsten zu relozierenden Wert
        add.l   D0,(A0)             ; Offset draufhauen
        bra.b   rel_loop            ; next Byte offset
add_254: 
	lea     254(A0),A0          ; 254 Bytes offset
        bra.b   rel_loop            ; Vielleicht hat's der n„chste.
end_rel:	
	rts
	
