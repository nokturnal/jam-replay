
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#include <mint/cookie.h>
#include <mint/osbind.h>
#include <mint/ostruct.h>

#include <stdio.h>
#include "machine.h"
#include "memory.h"

static const S8 *g_atariModels[MAX_ATARI_TYPE]={
  "Unknown",
  "Atari ST",
  "Atari Mega ST",
  "Atari ST Book",
  "Atari STe",
  "Atari Mega STe",
  "Atari TT",
  "Atari Falcon 030",
  "Atari Falcon CT60",
  "Milan",
  "Medusa",
  "Hades",
  "Eagle",
  "Firebee",
  "Emulator"
};

static const S8 *g_atariDisplay[MAX_DISP_TYPE]={
  "Unknown",
  "Shifter ST",
  "Shifter STe",
  "Shifter TT",
  "Videl",
  "SuperVidel",
  "ATI Radeon"
};

sMachineInfo _machineInfo;

const sMachineInfo *getMachineInfo(){
  return &_machineInfo;
}

const S8 *getMachineName(U16 type){
  if(type>=MAX_ATARI_TYPE)return 0;
  return g_atariModels[type];
}

const S8 *getDisplayName(U16 type){
  if(type>=MAX_DISP_TYPE) return 0;
  return g_atariDisplay[type];
}

void identifyMachine(){
  
}

 void checkMachine(){
 U32 mch,vdo,snd,cpu,fpu;
  
 sOSHEADER *pOS=0;
  
  //TOS version, GEMDOS/AES version
  _machineInfo.tos_version=pOS->os_version; 
  _machineInfo.os_config=pOS->os_conf; 
  _machineInfo.gemdos_ver=Sversion();
  
  //check cpu, fpu, machine type etc.. 
  //got cookie jar?
   if(getCookie(C__CPU, &cpu)==C_NOTFOUND){
    _machineInfo.cpu=0;
    _machineInfo.model=ST;
    _machineInfo.displayType=SHIFTER_ST;
    _machineInfo.snd=1;  	//assume only PSG
    _machineInfo.fpu=-1L;
   return;
  }
  
  if(getCookie(C__CPU, &cpu)==C_FOUND){
   _machineInfo.cpu=cpu;
  }
  
  if(getCookie(C__MCH, &mch)==C_FOUND){
    //check machine type
    U16 minor,major;
    
    major=(U16)((mch&0xFFFF0000UL)>>16);
    minor=(U16)(mch&0x0000FFFFUL);
    
    _machineInfo.mch=mch;
    
    switch(major){
      case 0:{
      //ST
	_machineInfo.model=ST;
      }break;
      case 1:{
      //STe or ST book or mega
	switch(minor){
	  case 0:{
	    _machineInfo.model=STE;
	  }break;
	  case 8:{
	    _machineInfo.model=ST_BOOK;
	  }break;
	  case 16:{
	     _machineInfo.model=MEGA_STE;
	  }break;
	}
      } break; 
      case 2:{
       //TT
       _machineInfo.model=TT;
      }break;	
      case 3:{
       //Falcon
	_machineInfo.model=F030;
      }break;	      
    }
      
  }else{
    _machineInfo.mch=-1L;
    _machineInfo.model=0;
  } 
  
  if(getCookie(C__VDO, &vdo)==C_FOUND){
    _machineInfo.vdo=vdo;
    //check shifter type
     U16 major=((mch&0xFFFF0000L)>>16);
     
     switch(major){
       case 0:{
	 _machineInfo.displayType=SHIFTER_ST;
      }break;
       case 1:{
	 _machineInfo.displayType=SHIFTER_STE;
      }break;
       case 2:{
	 _machineInfo.displayType=SHIFTER_TT;
      }break;
       case 3:{
	 _machineInfo.displayType=VIDEL;
      }break;
       default:{
	 _machineInfo.displayType=UNKNOWN_DISP;
      }break;
       
    };
    
  }else{
     _machineInfo.vdo=-1L;
  }
  
  if(getCookie(C__SND, &snd)==C_FOUND){
      _machineInfo.snd=snd;
      
  }else{
      _machineInfo.snd=-1L;
  }
  
  if(getCookie(C__FPU, &fpu)==C_FOUND){
      _machineInfo.fpu=fpu;
  }else{
        _machineInfo.fpu=-1L;
  }

  if(_machineInfo.model==F030){
   // check additional F030 stuff
   // accelerators, PCI bridges etc, graphics cards
   U32 ct60,pci;
   
   if(getCookie(C_CT60, &ct60)!=C_FOUND){
     
      //TODO: check supervidel
      #warning TODO add supervidel detection
      
      if(getCookie(C__PCI, &pci)==C_FOUND){
	//check graphics card availability
	if((unsigned long)Physbase()>=0x01000000UL){
	  _machineInfo.displayType=ATIRADEON;
	}
    } //check pci bios
   }//check ct60
  }
  
  //check available ST/TT-RAM
  _machineInfo.stram=getFreeMem(ST_RAM);
  _machineInfo.fastram=getFreeMem(FAST_RAM);
}

S16 getCookie (U32 target, U32 *p_value ){
U8 *oldssp=0;

sCOOKIE *cookie_ptr=(sCOOKIE *)0L;
oldssp = (Super(SUP_INQUIRE) ? NULL : (U8 *)Super(1L));

if((sCOOKIE **)0x5A0==0L) {
  if(oldssp) Super( oldssp );
  return C_NOTFOUND; //no cookie jar present, assume plain STf
}

cookie_ptr = *(sCOOKIE **)0x5A0;

if(cookie_ptr != NULL){
  do {
    if(cookie_ptr->cookie == target){
      if(p_value != NULL) *p_value = cookie_ptr->value;
      return C_FOUND;
    }
    
  } while((cookie_ptr++)->cookie != 0L);
}

 if(oldssp) Super( oldssp );

 return C_NOTFOUND;
}

sOSHEADER *getROMSysbase(){
  sOSHEADER *osret;
  U8 *savesp = (Super(SUP_INQUIRE) ? NULL : (U8 *)Super(SUP_SET));
  osret = (*(sOSHEADER **)0x4F2)->os_beg;
  if(savesp) Super( savesp );
  return osret;
}


