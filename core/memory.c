
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#include "memory.h"
#include "machine.h"

#include <mint/osbind.h>
#include <stdio.h>
#include <string.h>

/**
 * gets amount of free preferred memory type (ST/TT RAM).
 * @param memFlag memory allocation preference flag
 * @return 0L - if no memory available, 0L< otherwise
 */

#define STC 0


U32 getFreeMem(eMemoryFlag memFlag){
  void *pMem=0;
  const sMachineInfo *info=getMachineInfo();
  
  if(info->gemdos_ver>=0x1900){
    pMem=(void *)Mxalloc( -1L, memFlag);
  }else{
    //no fastram at all
    if(memFlag==FAST_RAM)
      pMem=(void *)0L;
    else
      pMem=(void *)Malloc(-1L);
  }

 return((U32)pMem);
}

void *cMemMove (void *pDest,const void *pSrc,tMEMSIZE iSize){
  return memmove(pDest,pSrc,iSize);
}


void *cMemCpy (void *pDest, const void *pSrc,tMEMSIZE iSize){

  U8 *pbDest=(U8 *)pDest;
  U8 *pbSrc=(U8 *)pSrc;
  
  if( (pbSrc<pbDest && (pbSrc + iSize)>pbDest ) || (pbDest<pbSrc && (pbDest +iSize) >pbSrc)){
    return cMemMove(pDest,pSrc,iSize);
  }
  
  return memcpy(pDest,pSrc,iSize);
}

void *cMemSet ( void *pSrc,S32 iCharacter,tMEMSIZE iNum){
  void *pPtr=0;
  pPtr=memset(pSrc,iCharacter,iNum);
  return pPtr;
}

int cMemCmp ( void *pSrc1, void *pSrc2, tMEMSIZE iNum){
  return memcmp(pSrc1,pSrc2,iNum);
}

void *cMemChr ( void *pSrc, S32 iCharacter, tMEMSIZE iNum){
  return memchr(pSrc,iCharacter,iNum);
}

void *cMallocEx(tMEMSIZE amount, U16 flag){
void *pMem=0;
  
#ifdef STC  
  const sMachineInfo *info=getMachineInfo();

  if(info->gemdos_ver>=0x1900)
    pMem=(void *)Mxalloc(amount,flag);
  else
    pMem=(void *)Malloc(amount);
#else
    pMem=(void *)malloc(amount);
#endif   
  return pMem;
}

void *cMalloc(tMEMSIZE amount){
void *pMem=0;
#ifdef STC  
pMem=(void *)Malloc(amount);
#else
    pMem=(void *)malloc(amount);
#endif
 return pMem;
}

void cFree(void **pPtr){
#ifdef STC  
if(*pPtr!=0){
 Mfree(*pPtr); *pPtr=0;
}
#else
if(*pPtr!=0){
 free(*pPtr);*pPtr=0;
}
#endif
}

void *cCalloc(tMEMSIZE nelements, tMEMSIZE elementSize){
  return calloc(nelements,elementSize);
}


void *cRealloc( void *pPtr, tMEMSIZE newSize){
 return realloc(pPtr,newSize);
}



S16 Mxmask (void){
    void *svStack;     /* Supervisor-Stack */
    S32 sRAM, sRAMg;   /* ST-RAM           */
    S32 aRAM, aRAMg;   /* Alternate RAM    */

    /*
    // Sample table of possible values:
    //           | newfashion  | oldfashion
    // sRAM aRAM | sRAMg aRAMg | sRAMg aRAMg
    //   1    0  |   1     0   |   1     1
    //   0    2  |   0     2   |   2     2
    //   1    2  |   1     2   |   3     3
    */

    svStack = (void *) Super( 0);  /* Disallow task-switching */

    sRAM  = (S32) Mxalloc( -1L, 0);
    sRAMg = (S32) Mxalloc( -1L, 0x40); /* In error case Mxalloc( -1, 3) */
    aRAM  = (S32) Mxalloc( -1L, 1);
    aRAMg = (S32) Mxalloc( -1L, 0x41); /* In error case Mxalloc( -1, 3) */

    SuperToUser( svStack);  	/* Permit task-switching */

    if (sRAM == -32) return (S16)0x0000;  /* Mxalloc is not implemented */

    else if ( ((sRAM + aRAM) == sRAMg) && ((sRAM + aRAM) == aRAMg) ) 
      return (S16)0x0003;  /* oldfashion Mxalloc() */
    else 
      return (S16)0xFFFF;

} /* Mxmask */

