
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details

#ifndef __C_VARS_H__
#define __C_VARS_H__

#include <stdlib.h>

typedef unsigned char 		U8;
typedef signed char 		S8;
typedef unsigned short int 	U16;
typedef signed short int 	S16;
typedef unsigned long U32;
typedef signed long 		S32;
typedef	float 			F32;
typedef float 			FP32;
typedef double 			FP64;
typedef unsigned int 		BOOL;
typedef signed long long	S64; //non-standard!
typedef unsigned long long 	U64; //non-standard!

typedef size_t tMEMSIZE;

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

void compilerSanityCheck(void);

//TODO: make it more cross-compilator friendly
// atm it's gcc specific
#ifndef STRUCT_PACK
#define PACK
#else
#define PACK __attribute__((packed))
#endif

//function pointers

typedef void (*funcPtrVoid)();
typedef U32 (*funcPtrInt)();


#endif
