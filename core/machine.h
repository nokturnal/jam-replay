
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#ifndef __MACHINE_H__
#define __MACHINE_H__

#include "core/c_vars.h"

#define PSG_BIT 0
#define STEREO_PLAYBACK 1 
#define DMA_REC_BIT 2
#define CODEC_BIT 3
#define DSP_BIT 4

typedef enum {
  UNKNOWN_MODEL=0,
  ST=1,
  MEGA_ST,
  ST_BOOK,
  STE,
  MEGA_STE,
  TT,
  F030,
  F060,
  MILAN,
  MEDUSA,
  HADES,
  EAGLE,
  FIREBEE,
  EMU,
  MAX_ATARI_TYPE
} eModel;

typedef enum {
  UNKNOWN_DISP=0,
  SHIFTER_ST,
  SHIFTER_STE,
  SHIFTER_TT,
  VIDEL,
  SUPERVIDEL,
  ATIRADEON,
  MAX_DISP_TYPE
} eDisplay;

typedef enum{
  MC68000=0,
  MC68010=10,
  MC68020=20,
  MC68030=30,
  MC68040=40,
  MC68060=60,
} eCPU;

typedef struct _cookie{
  U32 cookie;
  U32 value;
} sCOOKIE;

typedef struct _osheader {
  U16 os_entry;
  U16 os_version;
  void *reseth;
  struct _osheader *os_beg;
  U8 *os_end;
  U8 *os_rsv1;
  U8 *os_magic;
  S32 os_date;
  U16 os_conf;
  U16 os_dosdate;
  
  /* Available as of TOS 1.02 */
  U8 **p_root;
  U8 **p_kbshift;
  U8 **p_run;
  U8 *p_rsv2;
} sOSHEADER;

// custom structure for stroing current machine info

typedef struct _machineinfo {
  U16 tos_version;	// high byte is the major revision number, and the low byte is the minor revision number.
  U16 os_config;	// os_config>>1 (holds TOS version), (os_config&0%0000000000000001) 0-NTSC, 1-PAL
  U16 gemdos_ver;	// GEMDOS version
  U16 model; 		// enum from eModel
  U32 displayType; 	// enum from eDisplay
  U32 stram;		// available ST ram
  U32 fastram;		// available fast ram
  S32 cpu;		// value of _CPU cookie
  S32 mch;		// value of _MCH cookie
  S32 vdo;		// value of _VDO cookie
  S32 snd;		// value of _SND cookie
  S32 fpu;		// value of _FPU cookie
} sMachineInfo;

void checkMachine();
const S8 *getMachineName(U16 type);
const S8 *getDisplayName(U16 type);

S16 getCookie (U32 target, U32 *p_value );
const sMachineInfo *getMachineInfo();
sOSHEADER *getROMSysbase();


#endif

