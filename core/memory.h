
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#ifndef __MEMORY_H__
#define __MEMORY_H__

#include <mint/ostruct.h>
#include "core/c_vars.h"

// memory allocation preference 
// compatible with Mxalloc
typedef enum{
  ST_RAM=MX_STRAM,
  FAST_RAM=MX_TTRAM,
  PREFER_ST=MX_PREFSTRAM,
  PREFER_TT=MX_PREFTTRAM
} eMemoryFlag;

U32 getFreeMem(eMemoryFlag memFlag);
void *cMemMove (void *pDest,const void *pSrc,tMEMSIZE iSize);
void *cMemCpy (void *pDest, const void *pSrc,tMEMSIZE iSize);
void *cMemSet ( void *pSrc,S32 iCharacter,tMEMSIZE iNum);
int cMemCmp ( void *pSrc1, void *pSrc2, tMEMSIZE iNum);
void *cMemChr ( void *pSrc, S32 iCharacter, tMEMSIZE iNum);
void *cMallocEx(tMEMSIZE amount, U16 flag);
void *cMalloc(tMEMSIZE amount);
void cFree(void **pPtr);
void *cCalloc(tMEMSIZE nelements, tMEMSIZE elementSize);
void *cRealloc( void *pPtr, tMEMSIZE newSize);

/*
   Mxmask returns a bit-mask with which one should mask the mode
   WORD of a Mxalloc call if one wants to use protection bits.
   This is necessary as Mxalloc unfortunately returns erroneous
   results in some GEMDOS implementations when protection bits are
   specified, which can result in a system crash.
   (© 1994 Martin Osieka)

   Application example:
   mxMask = Mxmask();
   p = mxMask ? Mxalloc( size, 0x43 & mxMask) : Malloc( size); */

S16 Mxmask (void);

void relocateProgram(void *prgAddr);



#endif
