
// Jam replay api usage demo
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#include <mint/osbind.h>

#include "jam/jam.h"
#include "disc_io/disc_io.h"
#include "core/machine.h"

#include <stdio.h>

//loaded tune pointer
void *pMainTune=0;

//jam part
void *pPluginPtr=0;

// adjust those paths to your needs
#define CPU_PLUGIN_PATH "AHX_CPU.JAM"
#define DSP_PLUGIN_PATH "AHX_DSP.JAM"
#define TUNE "TUNE.AHX"

int main(int argc, char **argv){
U32 fileLen=0;

puts("Jam plugin replay demo \r\n");

//fill current machine info
Supexec(checkMachine);

const sMachineInfo *pInfo=getMachineInfo();

  if((pInfo->model==F030 ||pInfo->model==F060)){
      
	pPluginPtr=loadFile(DSP_PLUGIN_PATH, &fileLen);
      
     if(pPluginPtr==0){
	puts("Couldn't load the dsp plugin \r\n");
	Cconin();
	return -1;
      }
      
  }else{
      pPluginPtr=loadFile(CPU_PLUGIN_PATH, &fileLen);

      if(pPluginPtr==0){
      	puts("Couldn't load the cpu plugin \r\n");
	Cconin();
	return -1;
      }
      
  }
 
 
  //load tune
  pMainTune=loadFile(TUNE, &fileLen);
  
  if(pMainTune==0){
      puts("Couldn't load the tune \r\t");
      cFree((void**)&pPluginPtr);
      Cconin();
      return -1;
  } 

  
  if(initJamPlugin(pPluginPtr)>0){
      puts("Couldn't initialise plugin \r\n");
    return -1;
  }


  jamSetSong(pMainTune,1);
  jamPlay(); 
  
  puts("Press enter to quit..\r\n");
 
  //wait for keypress
  Cconin();

  //deinit plugin
  deinitJamPlugin();

 //free memory
  cFree(&pPluginPtr);
  cFree(&pMainTune);
  return 0;
}

 
