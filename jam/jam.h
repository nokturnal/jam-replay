#ifndef __JAM_H__
#define __JAM_H__

// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#include "core/c_vars.h"

typedef struct {
  U16 interfaceVersion;
  U8 date[12];
  U8 fileExt1[6];
  U8 fileExt2[6];
  U8 fileExt3[6];
  U8 fileExt4[6];
  U8 fileExt5[6];
  U8 fileExt6[6];
  U8 fileExt71[6];
  U8 fileExt8[6];
  U8 pluginName[128];
  U8 coderName[128];
  U8 email[128];
  U8 www[128];
  U8 comment[128];
  U16 isDsp;
  U16 support;
  U32 datastart;
  U16 supportsNextSongHook;
  U16 supportsName;
  U16 supportsComposer;
  U16 supportsSongCount;
  U16 supportsPreselect; 
  U16 supportsComment; 
  U16 supportsFastram;
} sJamPluginInfo;

typedef struct {
  U8 title[256];
  U8 composer[256];
  U8 ripper[256];
  U8 email[256];
  U8 www[256];
  U8 comments[256];
  U16 songHz;
  U16 songCount; 
  U16 songPreselect; 
  U16 isYMsong;
  U32 dmaHz;
  U8 filename[256];
  U16 playtime_min[99];
  U16 playtime_sec[99];
} sJamSongInfo;

S32 initJamPlugin(void *pPlugin);
void deinitJamPlugin();

void jamSetSong(void *songPtr,U16 songNb);
void jamPlay();
void jamStop();


#endif
