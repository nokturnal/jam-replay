
// jam c replay routines
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#include "jam.h"
#include "core/machine.h"
#include "core/memory.h"

#include <stdio.h>
#include <mint/osbind.h>

enum _JamMsg{
  JAM_INFO=1,
  JAM_INIT=2,
  JAM_ACTIVATE=3,
  JAM_SONGSELECT=4,
  JAM_SONGINFO=5,
  JAM_PLAY=6,
  JAM_STOP=7,
  JAM_DEACTIVATE=8,
  JAM_DEINIT=9,
  JAM_SONG_NEXT_HOOK=0xA,
  JAM_LOG_HOOK=0xB,
  JAM_ALERT_HOOK=0xC
} eJamMsg;

//initScheme
enum{
  JAM_ST=0,
  JAM_STE=1,
  JAM_TT=2,  	//not sure
  JAM_MEGASTE=3,
  JAM_F030=4
} eJamInitScheme;

extern void *activePlugin;

sJamSongInfo songInfo;

extern void *sendJamCmd(void *par0,void *par1,U32 msg,U32 val);

static void logHook(){
  puts("LogHook\r\t");
}
static void alertHook(){
  puts("alertHook\r\t");
  
}

static U16 model;

void initPlugin(){
const sMachineInfo *pInfo=getMachineInfo();
void *ret=0;
U32 val;

model=pInfo->model;

  switch(model){
    case ST:
    case MEGA_ST:
    case ST_BOOK:{
	val=JAM_ST;
    } break;
    case STE:{
	val=JAM_STE;
    }
    break;
    case MEGA_STE:{
	val=JAM_MEGASTE;
    }
    break;
  case TT:{
   	val=JAM_TT;
   }
   break;
   case F030:
   case F060:{
	val=JAM_F030;
   }
   break;
   default:{
      val=JAM_ST;
   }
   break;
  };

 ret=sendJamCmd((void *)0L,(void *)0L,JAM_INIT,val);
  
}

U16 finishedReplayFlag;

void jamPlay(){
  S32 usp=0;
  usp=Super(0L);
  sendJamCmd((void *)0L,(void *)0L,JAM_PLAY,0);
  SuperToUser(usp);
}

  
void jamSetSong(void *songPtr,U16 songNb){
  S32 usp=0;
  usp=Super(0L);
  
  sendJamCmd(songPtr,(void *)0L,JAM_SONGSELECT,songNb);
  sendJamCmd(&songInfo,songPtr,JAM_SONGINFO,0);
  
  sendJamCmd(songPtr,(void *)0L,JAM_SONGSELECT,songNb);
  sendJamCmd(&songInfo,(void *)0L,JAM_SONGINFO,0);
  
  SuperToUser(usp);
}

void jamStop(){
  S32 usp=0;
  usp=Super(0L);
  sendJamCmd((void *)0L,(void *)0L,JAM_STOP,0);
  SuperToUser(usp);
}

S32 initJamPlugin(void *pPlugin){
void *ret=0;
S32 usp=0;

relocateProgram(pPlugin);

usp=Super(0L);

 activePlugin=pPlugin;
 finishedReplayFlag=0;
 
 initPlugin(); 
 
  const sJamPluginInfo *plugiInfo=(const sJamPluginInfo *)sendJamCmd((void *)0L,(void *)0L,JAM_INFO,0);

  ret=sendJamCmd((void *)0L,(void *)0L,JAM_ACTIVATE,0);
  ret=sendJamCmd(logHook,(void *)0L,JAM_LOG_HOOK,0);
  ret=sendJamCmd(alertHook,(void *)0L,JAM_ALERT_HOOK,0);
  ret=sendJamCmd(&finishedReplayFlag,(void *)0L,JAM_SONG_NEXT_HOOK,0);
  
  SuperToUser(usp);
  
  return 0;
}

void deinitJamPlugin(){
  S32 usp=Super(0L);

  sendJamCmd((void *)0L,(void *)0L,JAM_STOP,0);
  sendJamCmd((void *)0L,(void *)0L,JAM_DEACTIVATE,0);
  sendJamCmd((void *)0L,(void *)0L,JAM_DEINIT,0);

  activePlugin=(void *)0L;
  
  SuperToUser(usp);
}


