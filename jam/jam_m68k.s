
	   XDEF	_sendJamCmd
	   XREF	_activePlugin
	   
_sendJamCmd:
	  
	  move.l	4(sp),a0  ;data pointer	
	  move.l	8(sp),a1  ;data pointer	
	  
	  move.l	12(sp),d1  ;MSG
	  move.l	16(sp),d0 ;parameter
	  
	  move.l	_activePlugin,a2	
	  jsr		(a2)
	  move.l	a0,d0	 ;return value to d0
	  
	  rts
	  
	  BSS
    
_activePlugin: 	  ds.l	1
	  

	  