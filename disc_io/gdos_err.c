
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details


#include "gdos_err.h"

static S16 g_lastGDOSerror=0;

static const U8 *g_arGEMDOSerror[71]= { 
  "No error.",\
  "Error.",\
  "Drive not ready.",\
  "Unknown command.",\
  "CRC (checksum) error.",\
  "Bad request.",\
  "Seek error.",\
  "Unknown media.",\
  "Sector not found.",\
  "Out of paper.",\
  "Write fault.",\
  "Read fault.",\
  "NVM checksum error.",\
  "Write protected media.",\
  "Media change detected.",\
  "Unknown device.",\
  "Bad sectors on format.",\
  "Insert other disk (request).",\
  "Invalid GEMDOS function number.",\
  "File not found.",\
  "Path not found.",\
  "Handle pool exhausted.",\
  "Access denied.",\
  "Invalid handle.",\
  "Insufficient memory.",\
  "Invalid memory block address.",\
  "Invalid drive specification.",\
  "Not the same drive.",\
  "No more files.",\
  "Record is already locked.",\
  "Invalid lock removal request.",\
  "Argument range error.",\
  "GEMDOS internal error.",\
  "Invalid executable file format.",\
  "Memory block growth failure.",\
  "Too many symbolic links.",\
  "Mount point crossed."
};


const U8 *getGemdosError(S16 iErr){
  
  switch(iErr){
        case GDOS_OK:
            g_lastGDOSerror=GDOS_OK;
            return(g_arGEMDOSerror[GDOS_OK]);
        break;
        case GDOS_ERR:
            g_lastGDOSerror=GDOS_ERR;
            return(g_arGEMDOSerror[GDOS_ERR]);
        break; /*Error*/
        case GDOS_DRIVE_NOT_READY:
            g_lastGDOSerror=GDOS_DRIVE_NOT_READY;
            return(g_arGEMDOSerror[GDOS_DRIVE_NOT_READY]);
        break;  /*Drive not ready*/
        case GDOS_UNKNOWN_COMMAND:
            g_lastGDOSerror=GDOS_UNKNOWN_COMMAND;
            return((const U8 *)g_arGEMDOSerror[GDOS_UNKNOWN_COMMAND]);
        break;  /*Unknown command*/
        case GDOS_CRC_ERROR:
            g_lastGDOSerror=GDOS_CRC_ERROR;
            return(g_arGEMDOSerror[GDOS_CRC_ERROR]);
        break;  /*CRC (checksum) error*/
        case GDOS_BAD_REQ:
            g_lastGDOSerror=GDOS_BAD_REQ;
            return(g_arGEMDOSerror[GDOS_BAD_REQ]);
        break;  /*Bad request*/
        case GDOS_SEEK_ERROR:
            g_lastGDOSerror=GDOS_SEEK_ERROR;
            return((const U8 *)g_arGEMDOSerror[GDOS_SEEK_ERROR]);
        break;  /*Seek error*/
        case GDOS_UNKNOWN_MEDIA:
            g_lastGDOSerror=GDOS_UNKNOWN_MEDIA;
            return(g_arGEMDOSerror[GDOS_UNKNOWN_MEDIA]);
        break;  /*Unknown media*/
        case GDOS_SECTOR_NOT_FOUND:
            g_lastGDOSerror=GDOS_SECTOR_NOT_FOUND;
            return(g_arGEMDOSerror[GDOS_SECTOR_NOT_FOUND]);
        break;  /*Sector not found*/
        case GDOS_OUT_OF_PAPER:
            g_lastGDOSerror=GDOS_OUT_OF_PAPER;
            return(g_arGEMDOSerror[GDOS_OUT_OF_PAPER]);
        break;  /*Out of paper*/
        case GDOS_WRITE_FAULT:
            g_lastGDOSerror=GDOS_WRITE_FAULT;
            return(g_arGEMDOSerror[GDOS_WRITE_FAULT]);
        break;  /*Write fault*/
        case GDOS_READ_FAULT:
            g_lastGDOSerror=GDOS_READ_FAULT;
            return(g_arGEMDOSerror[GDOS_READ_FAULT]);
        break;  /*Read fault*/
        case GDOS_NVM_CHESUM_ERR:
            g_lastGDOSerror=GDOS_NVM_CHESUM_ERR;
            return(g_arGEMDOSerror[GDOS_NVM_CHESUM_ERR]);
        break;  /*NVM checksum error*/
        case GDOS_WRITE_PROTECTED_MEDIA:
            g_lastGDOSerror=GDOS_WRITE_PROTECTED_MEDIA;
            return(g_arGEMDOSerror[GDOS_WRITE_PROTECTED_MEDIA]);
        break;  /*Write protected media*/
        case GDOS_MEDIA_CHANGE_DETECTED:
            g_lastGDOSerror=GDOS_MEDIA_CHANGE_DETECTED;
            return(g_arGEMDOSerror[GDOS_MEDIA_CHANGE_DETECTED]);
        break;  /*Media change detected*/
        case GDOS_UNKNOWN_DEVICE:
            g_lastGDOSerror=GDOS_UNKNOWN_DEVICE;
            return(g_arGEMDOSerror[GDOS_UNKNOWN_DEVICE]);
        break;  /*Unknown device*/
        case GDOS_BAD_SECTOR_ON_FORMAT:
            g_lastGDOSerror=GDOS_BAD_SECTOR_ON_FORMAT;
            return(g_arGEMDOSerror[GDOS_BAD_SECTOR_ON_FORMAT]);
        break;  /*Bad sectors on format*/
        case GDOS_INSERT_OTHER_DISK_REQ:
            g_lastGDOSerror=GDOS_INSERT_OTHER_DISK_REQ;
            return(g_arGEMDOSerror[GDOS_INSERT_OTHER_DISK_REQ]);
        break;  /*Insert other disk (request)*/
        case GDOS_INVALID_GDOS_FUNC_NB:
            g_lastGDOSerror=GDOS_INVALID_GDOS_FUNC_NB;
            return(g_arGEMDOSerror[GDOS_INVALID_GDOS_FUNC_NB]);
        break;  /*Invalid GEMDOS function number*/
        case GDOS_FILE_NOT_FOUND:
            g_lastGDOSerror=GDOS_FILE_NOT_FOUND;
            return(g_arGEMDOSerror[GDOS_FILE_NOT_FOUND]);
        break;  /*File not found*/
        case GDOS_PATH_NOT_FOUND:
            g_lastGDOSerror=GDOS_PATH_NOT_FOUND;
            return(g_arGEMDOSerror[GDOS_PATH_NOT_FOUND]);
        break;  /*Path not found*/
        case GDOS_HANDLE_POOL_EXHAUSTED:
            g_lastGDOSerror=GDOS_HANDLE_POOL_EXHAUSTED;
            return(g_arGEMDOSerror[GDOS_HANDLE_POOL_EXHAUSTED]);
        break;  /*Handle pool exhausted*/
        case GDOS_ACCESS_DENIED:
            g_lastGDOSerror=GDOS_ACCESS_DENIED;
            return(g_arGEMDOSerror[GDOS_ACCESS_DENIED]);
        break;  /*Access denied*/
        case GDOS_INVALID_HANDLE:
            g_lastGDOSerror=GDOS_INVALID_HANDLE;
            return(g_arGEMDOSerror[GDOS_INVALID_HANDLE]);
        break;  /*Invalid handle*/
        case GDOS_INSUFFICIENT_MEMORY:
            g_lastGDOSerror=GDOS_INSUFFICIENT_MEMORY;
            return(g_arGEMDOSerror[GDOS_INSUFFICIENT_MEMORY]);
        break;  /*Insufficient memory*/
        case GDOS_INVALID_MEM_BLOCK_ADDR:
            g_lastGDOSerror=GDOS_INVALID_MEM_BLOCK_ADDR;
            return(g_arGEMDOSerror[GDOS_INVALID_MEM_BLOCK_ADDR]);
        break;  /*Invalid memory block address*/
        case GDOS_INVALID_DRIVE_SPEC:
            g_lastGDOSerror=GDOS_INVALID_DRIVE_SPEC;
            return(g_arGEMDOSerror[GDOS_INVALID_DRIVE_SPEC]);
        break;  /*Invalid drive specification*/
        case GDOS_NOT_THE_SAME_DRIVE:
            g_lastGDOSerror=GDOS_NOT_THE_SAME_DRIVE;
            return(g_arGEMDOSerror[GDOS_NOT_THE_SAME_DRIVE]);
        break;  /*Not the same drive*/
        case GDOS_NO_MORE_FILES:
            g_lastGDOSerror=GDOS_NO_MORE_FILES;
            return(g_arGEMDOSerror[GDOS_NO_MORE_FILES]);
        break;  /*No more files*/
        case GDOS_RECORD_LOCKED:
            g_lastGDOSerror=GDOS_RECORD_LOCKED;
            return(g_arGEMDOSerror[GDOS_RECORD_LOCKED]);
        break;  /*Record is already locked*/
        case GDOS_INVALID_LOCK_REMOVAL_REQ:
            g_lastGDOSerror=GDOS_INVALID_LOCK_REMOVAL_REQ;
            return(g_arGEMDOSerror[GDOS_INVALID_LOCK_REMOVAL_REQ]);
        break;  /*Invalid lock removal request*/
        case GDOS_ARG_RANGE_ERR:
            g_lastGDOSerror=GDOS_ARG_RANGE_ERR;
            return(g_arGEMDOSerror[GDOS_ARG_RANGE_ERR]);
		break;  /*Argument range error*/
        case GDOS_INTERNAL_ERR:
            g_lastGDOSerror=GDOS_INTERNAL_ERR;
            return((const U8 *)g_arGEMDOSerror[GDOS_INTERNAL_ERR]);
        break;  /*GEMDOS internal error*/
        case GDOS_INVALID_EXE_FMT:
            g_lastGDOSerror=GDOS_INVALID_EXE_FMT;
            return(g_arGEMDOSerror[GDOS_INVALID_EXE_FMT]);
        break;  /*Invalid executable file format*/
        case GDOS_MEM_BLOCK_GROWTH_FAIL:
            g_lastGDOSerror=GDOS_MEM_BLOCK_GROWTH_FAIL;
            return(g_arGEMDOSerror[GDOS_MEM_BLOCK_GROWTH_FAIL]);
        break;  /*Memory block growth failure*/
        case GDOS_TOO_MANY_SYMLINKS:
            g_lastGDOSerror=GDOS_TOO_MANY_SYMLINKS;
            return(g_arGEMDOSerror[GDOS_TOO_MANY_SYMLINKS]);
        break;  /*Too many symbolic links*/
        case GDOS_MOUNT_POINT_CROSSED:
            g_lastGDOSerror=GDOS_MOUNT_POINT_CROSSED;
            return(g_arGEMDOSerror[GDOS_MOUNT_POINT_CROSSED]);
        break;  /*Mount point crossed*/
        default:
            /* unknown error */
            g_lastGDOSerror=GDOS_ERR;
            return(g_arGEMDOSerror[GDOS_ERR]);
        break;
    }
 /* to make compiler happy ..*/
 return(g_arGEMDOSerror[1]);
}
