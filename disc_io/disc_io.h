
// (c)2011-2012 Pawel Goralski
// http://nokturnal.pl
// see licence.txt for details

#ifndef __DISCIO_H__
#define __DISCIO_H__

#include "core/c_vars.h"
#include "core/memory.h"
#include "gdos_err.h"

/*XBIOS input/output devices*/
#define XB_DEV_AUX 0	/* Centronics interface */
#define XB_DEV_KBD 1    /* Keyboard port */
#define XB_DEV_MIDI 2	/* MIDI interface */
 
/**
 * loads file to specific type of memory(ST/TT RAM). (makes memory allocation)
 * 
 * @param szFileName NULL terminated full path with name
 * @param memFlag memory allocation preference flag
 * @param fileLenght file lenght in bytes
 * @return NULL - if error occured,
 * valid pointer - if file was loaded.
 */

void *loadFileEx(const U8 *szFileName, eMemoryFlag memFlag, U32 *fileLenght);

/**
 * loads file to memory (makes memory allocation)
 * 
 * @param szFileName NULL terminated full path with name
 * @param fileLenght file lenght in bytes
 * @return NULL - if error occured,
 * valid pointer - if file was loaded.
 */
void *loadFile(const U8 *szFileName,U32 *fileLenght);


/**
 * loads file to memory to specified buffer
 * 
 * @param szFileName NULL terminated full path with name
 * @param destBufSize Destination buffer size in bytes. If file size is greater than destination buffer
 * then only destBufSize is copied to prevent buffer overrun.
 * @param buffer pointer to preallocated destination buffer 
 * @return value >0 - if error occured,
 */

U16 loadFileToBuffer(const U8 *szFileName,U32 destBufSize, void *buffer);


#endif

