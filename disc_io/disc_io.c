// disc i/o routines
// (c)2011 Pawel Goralski
// see licence.txt for details

#include <mint/ostruct.h>
#include <mint/osbind.h>
#include <stdio.h>
#include "disc_io.h"
#include "../core/memory.h"

void *loadFile(const U8 *szFileName, U32 *fileLenght){
S32 fileHandle;
void *pData=NULL;
_DTA *oldDTA=0;
_DTA tempDta;
S16 iRet=0;
U32 lRet=0L;

fileHandle = Fopen( szFileName, FO_READ );
*fileLenght=0L;
oldDTA=Fgetdta();
      cMemSet((void *)&tempDta,0,sizeof(_DTA));

if((fileHandle)>0L){
   
    Fsetdta(&tempDta);
    iRet=Fsfirst(szFileName, 0 );
//TODO: replace 0 with E_OK etc
    if(iRet==0){
    /* file found */
	 
    *fileLenght=tempDta.dta_size;

    /* allocate buffer */
     pData=(void *)cMalloc((tMEMSIZE)(*fileLenght));
     
     if(pData!=NULL){
       
      cMemSet(pData,0,(*fileLenght));

      lRet=Fread( (int)fileHandle, (tMEMSIZE)(*fileLenght), pData );

      /* not all data being read */
      if(lRet!=(*fileLenght)){
	puts((const char *)"Fatal error, Couldn't read the whole file.\n");
	
	/* so we have error, free up memory */
	cFree(&pData);
      }
        Fclose((int)fileHandle);
	Fsetdta(oldDTA);
	return (pData);
     }else{
      /*no memory available */
      puts((const char *)"Not enough memory to load file.\n");
      Fclose((int)fileHandle);
      Fsetdta(oldDTA);
      return NULL;
     }
     
    }else{
      Fclose((int)fileHandle);
      /* file not found */
      Fsetdta(oldDTA);
      puts((const char *)getGemdosError(iRet));
      return NULL;
     }
    }else{
     /* print GEMDOS error code */
     puts((const char *)getGemdosError((S16)fileHandle));
     return NULL;
    }
}

/* loads file to specified type memory */
void *loadFileEx(const U8 *szFileName, eMemoryFlag memFlag,  U32 *fileLenght){
S32 fileHandle;
_DTA *oldDTA=0;
_DTA tempDta;
  
void *pData=NULL;
S16 iRet=0;
U32 lRet=0L;
 oldDTA=Fgetdta();
   cMemSet((void *)&tempDta,0,sizeof(_DTA));

    fileHandle = Fopen( szFileName, FO_READ );
    *fileLenght=0L;

    if((fileHandle)>0L){
   
    Fsetdta(&tempDta);
     
    iRet=Fsfirst( szFileName, 0 );

    if(iRet==0){
    /* file found */
	 
    *fileLenght=oldDTA->dta_size;

    /* allocate buffer */
     pData=(void *)cMallocEx((tMEMSIZE)(*fileLenght),memFlag);
     
     if(pData!=NULL){
       
      cMemSet(pData,0,(*fileLenght));

      lRet=Fread( (int)fileHandle, (tMEMSIZE)(*fileLenght), pData );

      /* not all data being read */
      if(lRet!=(*fileLenght)){
	puts((const char *)"Fatal error, Couldn't read the whole file.\n");
	
	/* so we have error, free up memory */
	cFree(&pData);
      }
        
        Fclose((int)fileHandle);
	Fsetdta(oldDTA);
	return (pData);
     }
     else{
      /*no memory available */
      puts((const char *)"Not enough memory to load file.\n");
      Fclose((int)fileHandle);
      Fsetdta(oldDTA);
      return NULL;
     }
    }else{
      Fclose((int)fileHandle);
       Fsetdta(oldDTA);
      /* file not found */
      puts((const char *)getGemdosError(iRet));
      return NULL;
     }
     
      Fsetdta(oldDTA);
    }
    else {
        /* print GEMDOS error code */
        puts((const char *)getGemdosError((S16)fileHandle));
        return NULL;
    }
}

U16 loadFileToBuffer(const U8 *szFileName,U32 destBufSize, void *buffer){
  S32 fileHandle;
  _DTA *oldDTA=0;
  _DTA tempDta;
  
  
  S16 iRet=0;
  U32 fileLenght=0L,lRet=0L;
  oldDTA=Fgetdta();
  cMemSet((void *)&tempDta,0,sizeof(_DTA));
  
  fileHandle = Fopen( szFileName, FO_READ);
 
  if((fileHandle)>0L){
    //file opened 
    Fsetdta(&tempDta);
    iRet=Fsfirst( szFileName, 0 );

    if(iRet==0){
	/* file found */
	fileLenght=tempDta.dta_size;

	if(fileLenght>destBufSize){
	  //buffer too small
	  puts((const char *)"loadFileToBuffer(), destination buffer too small.\n");
	  Fclose((int)fileHandle);
	  return 1;
	}
    
	cMemSet(buffer,0,destBufSize);

	lRet=Fread( (int)fileHandle, (tMEMSIZE)(fileLenght), buffer );

	/* not all data being read */
	if(lRet!=fileLenght){
	  puts((const char *)"Fatal error, Couldn't read the whole file.\n");
	  
	  Fclose((int)fileHandle);
	   Fsetdta (oldDTA);
	  return 1;
	}
      } else{
	/* file not found */
	Fclose((int)fileHandle);
	puts((const char *)getGemdosError(iRet));
	return 1;
      }
      
      Fclose((int)fileHandle);
      Fsetdta (oldDTA);
    }
    else {
        /* print GEMDOS error code */
        puts((const char *)getGemdosError((S16)fileHandle));
        return 1;
    }

 return 0; 
}

