# JAM replay

Original Jam plugin api and Atari Jam player (c) Cream https://creamhq.de. 

Jam plugin C API and all utility functions (c) 2011-2012 Pawel Goralski 
https://nokturnal.pl, see licence.txt 

Custom gcc startup (c)2011 Mariusz Buras, C 
modifications: Pawel Goralski, published under MIT licence, see 
licence.txt 

Short description: Here is example of Jam C API usage and precompiled 
binary. Plugin's and tune is not included. Get plugins from official 
Cream web site: https://creamhq.de . Tunes can be found on the internet. 

Check out the full article about Jam Player API on Atari 16/32 bit 
development wiki: https://bus-error.nokturnal.pl 

Compilation info: 
To compile the example program you will need: 
* recent version of VASM https://sun.hasenbraten.de/vasm in your system 
path, 

* m68k cross tools from 
http://vincent.riviere.free.fr/soft/m68k-atari-mint, 

* Scons https://www.scons.org. 

To build example type in 'scons' from console. To make clean type in 
terminal 'scons -c'. And that's all. If you want to compile example 
natively then you're welcome to write your own Makefile ;). 








