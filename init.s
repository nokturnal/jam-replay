
;initialisation functions
	XDEF _keyclickOff
	XDEF _keyclickOn
	XDEF _saveSTpal
	XDEF _restoreSTpal
	XDEF _saveSTscreenMode
	XDEF _restoreSTscreenMode
	XDEF _oldSTresolution

	XDEF _saveMFP
	XDEF _restoreMFP
	XDEF _physBuf
	XDEF _logBuf
	XDEF _screenSwap

	XDEF _turnOffSpeaker

SPEAKER equ $6060

_turnOffSpeaker:
	move.l #$0e0e0505 | SPEAKER,$ffff8800.w
	rts

_keyclickOff:	
	move.b	$484.w,click
	clr.b	$484.w
	rts
	
_keyclickOn:	
	move.b	click,$484.w
	rts

_saveSTpal:	
	movem.l	$ffff8240.w,d0-d7
	movem.l	d0-d7,oldpal
	rts

_restoreSTpal:	
	movem.l	oldpal,d0-d7
	movem.l	d0-d7,$ffff8240.w
	rts

_saveSTscreenMode:
;SAVE REZ
	move.w	#4,-(sp)
	trap	#14
	addq.l	#2,sp
	move.w	d0,_oldSTresolution

;SAVE PHYS AND LOG

;phys
	move.w	#2,-(sp)
	trap	#14
	addq.l	#2,sp
	move.l	d0,oldphys
	
;log
	move.w	#3,-(sp)
	trap	#14
	addq.l	#2,sp
	move.l	d0,oldlog
	rts

_restoreSTscreenMode:
	move.w	_oldSTresolution,-(sp)
	move.l	oldphys,-(sp)
	move.l	oldlog,-(sp)
	move.w	#5,-(sp)
	trap	#14
	adda.w	#12,sp
	rts
	
	even

		
_restoreMFP:
	move.l	a0,-(sp)
	move.w	sr,-(sp)
	move.w	#$2700,sr

	lea	mfp_regs(pc),a0
	move.b	(a0)+,$fffffa01.w
	move.b	(a0)+,$fffffa03.w
	move.b	(a0)+,$fffffa05.w
	move.b	(a0)+,$fffffa07.w
	move.b	(a0)+,$fffffa09.w
	move.b	(a0)+,$fffffa0b.w
	move.b	(a0)+,$fffffa0d.w
	move.b	(a0)+,$fffffa0f.w
	move.b	(a0)+,$fffffa11.w
	move.b	(a0)+,$fffffa13.w
	move.b	(a0)+,$fffffa15.w
	move.b	(a0)+,$fffffa17.w
	move.b	(a0)+,$fffffa19.w
	move.b	(a0)+,$fffffa1b.w
	move.b	(a0)+,$fffffa1d.w
	move.b	(a0)+,$fffffa1f.w
	move.b	(a0)+,$fffffa21.w
	move.b	(a0)+,$fffffa23.w
	move.b	(a0)+,$fffffa25.w
	add.l	#1,a0
	move.l	(a0)+,$94.w
	move.l	(a0)+,$110.w
	move.l	(a0)+,$114.w
	move.l	(a0)+,$120.w
	move.l	(a0)+,$134.w
	move.l	(a0)+,$70.w
	move.l	(a0)+,$118.w
	move.b #$C0,$FFFFFA23.W
	move.w	(sp)+,sr
	move.l	(sp)+,a0
	rts

_saveMFP:
	move.l	a0,-(sp)
	move.w	sr,-(sp)
	move.w	#$2700,sr

	lea	mfp_regs(pc),a0
	move.b	$fffffa01.w,(a0)+
	move.b	$fffffa03.w,(a0)+
	move.b	$fffffa05.w,(a0)+
	move.b	$fffffa07.w,(a0)+
	move.b	$fffffa09.w,(a0)+
	move.b	$fffffa0b.w,(a0)+
	move.b	$fffffa0d.w,(a0)+
	move.b	$fffffa0f.w,(a0)+
	move.b	$fffffa11.w,(a0)+
	move.b	$fffffa13.w,(a0)+
	move.b	$fffffa15.w,(a0)+
	move.b	$fffffa17.w,(a0)+
	move.b	$fffffa19.w,(a0)+
	move.b	$fffffa1b.w,(a0)+
	move.b	$fffffa1d.w,(a0)+
	move.b	$fffffa1f.w,(a0)+
	move.b	$fffffa21.w,(a0)+
	move.b	$fffffa23.w,(a0)+
	move.b	$fffffa25.w,(a0)+
	add.l	#1,a0
	move.l	$094.w,(a0)+
	move.l	$110.w,(a0)+
	move.l	$114.w,(a0)+
	move.l	$120.w,(a0)+
	move.l	$134.w,(a0)+
	move.l	$70.w,(a0)+
	move.l	$118.w,(a0)+
	move.b 	#$C0,$FFFFFA23.w
	move.w	(sp)+,sr

	move.l	(sp)+,a0
	rts


;Falcon videl set up	
save_videl:
	bsr.w	wait_vbl			; avoid flicking
	
	lea	$ffff9800.w,a0			; save falcon palette
	lea	falc_pal,a1			; 
	moveq	#128-1,d7			; 

.save_loop:					; 
	move.l	(a0)+,(a1)+			; 
	move.l	(a0)+,(a1)+			; 
	dbra	d7,.save_loop			; 
	
	lea	oldpal,a0
	movem.l	$ffff8240.w,d0-d7		; save st palette
	movem.l	d0-d7,(a1)			; 

	lea	videl_regs,a0
	move.l	$ffff8200.w,(a0)+		; vidhm
	move.w	$ffff820c.w,(a0)+		; vidl
	
	move.l	$ffff8282.w,(a0)+		; h-regs
	move.l	$ffff8286.w,(a0)+		; 
	move.l	$ffff828a.w,(a0)+		; 
	
	move.l	$ffff82a2.w,(a0)+		; v-regs
	move.l	$ffff82a6.w,(a0)+		; 
	move.l	$ffff82aa.w,(a0)+		; 
	
	move.w	$ffff82c0.w,(a0)+		; vco
	move.w	$ffff82c2.w,(a0)+		; c_s
	
	move.l	$ffff820e.w,(a0)+		; offset
	move.w	$ffff820a.w,(a0)+		; sync
	
	move.b  $ffff8256.w,(a0)+		; p_o
	
	cmpi.w   #$b0,$ffff8282.w		; st(e) / falcon test
	sle	(a0)+				; it's a falcon resolution
	
	move.w	$ffff8266.w,(a0)+		; f_s
	move.w	$ffff8260.w,(a0)+		; st_s

	rts
	
	
_restore_videl:
	bsr.w	wait_vbl			; avoid flickering
	lea	videl_regs,a0
	
	move.l	(a0)+,$ffff8200.w		; videobase_address:h&m
	move.w	(a0)+,$ffff820c.w		; l
	
	move.l	(a0)+,$ffff8282.w		; h-regs
	move.l	(a0)+,$ffff8286.w		; 
	move.l	(a0)+,$ffff828a.w		; 
	
	move.l	(a0)+,$ffff82a2.w		; v-regs
	move.l	(a0)+,$ffff82a6.w		; 
	move.l	(a0)+,$ffff82aa.w		; 
	
	move.w	(a0)+,$ffff82c0.w		; vco
	move.w	(a0)+,$ffff82c2.w		; c_s
	
	move.l	(a0)+,$ffff820e.w		; offset
	move.w	(a0)+,$ffff820a.w		; sync
	
	move.b  (a0)+,$ffff8256.w		; p_o
	
	tst.b   (a0)+   			; st(e) compatible mode?
	bne.b   .restore_ok			; yes
        	
	move.w  (a0),$ffff8266.w		; falcon-shift
	
	move.w  $ffff8266.w,-(sp)		; Videl patch
	bsr.w	wait_vbl			; to avoid monochrome
	clr.w   $ffff8266.w			; sync errors
	bsr.w	wait_vbl			; (ripped from

	move.w	(sp)+,$ffff8266.w		; FreeMiNT kernel)
	bra.b	.restored

.restore_ok:
	; clr.w	$ffff8266.w
	move.w	(a0)+,$ffff8266.w		; falcon-shift
	move.w  (a0),$ffff8260.w		; st-shift
	lea	videl_regs,a0
	move.w	32(a0),$ffff82c2.w		; c_s
	move.l	34(a0),$ffff820e.w		; offset		
.restored:
	lea	falc_pal,a0			; restore falcon palette
	lea	$ffff9800.w,a1			; 		
	moveq	#128-1,d7			; 
.restore_loop:					; 
	move.l	(a0)+,(a1)+			; 
	move.l	(a0)+,(a1)+			; 
	dbra	d7,.restore_loop			; 
	
	movem.l	oldpal,d0-d7			; restore st palette
	movem.l	d0-d7,$ffff8240.w		; 
	
	rts

wait_vbl:
	move.l	a0,-(sp)
	move.w	#$25,-(sp)			; Vsync()
	trap	#14				; 
	addq.l	#2,sp				; 
	movea.l	(sp)+,a0
	rts
	
	BSS

_physBuf:	ds.l	1
_logBuf:	ds.l	1

_oldSTresolution: ds.w	1

	even
oldphys:	ds.l	1
oldlog:		ds.l	1

oldpal:		ds.w	16
falc_pal:	ds.l	256	; old colours (falcon)

		even

mfp_regs:	ds.b	56
videl_regs:	ds.b	32+12+2		; videl save

;key clicks $484 vector
click:		ds.b	1

	

	
